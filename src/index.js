function ScrollToElement(idscroll,idbotton){
    if(typeof idscroll === "string" && typeof idbotton==="string"){

        let scroll = document.querySelector(idscroll);
        let botton = document.querySelector(idbotton);

        if(scroll && botton){
            scroll.addEventListener("click", function(){
                botton.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                    inline: "nearest",
                })
            })
        }
    }
}
ScrollToElement("#click", "#footer");


function ClickInMenu (idclick,idmenu){
    if(typeof idclick === "string" && typeof idmenu === "string"){

        let click = document.querySelector(idclick);
        let menu = document.querySelector(idmenu);

        if(click && menu){
            click.addEventListener("click", function(){
                menu.classList.toggle("active")
            })
        }
    }
}

ClickInMenu("#scroll","#menu");